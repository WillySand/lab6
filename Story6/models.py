from django.db import models
from datetime import datetime
from django.utils import timezone

class Status(models.Model):
        
    status = models.CharField(max_length=300, help_text='Status')
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)
