from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status

def home(request):
    form = StatusForm()
    statuses = Status.objects.order_by("date","time")
    response = {
        'status' : statuses,
    }
    if request.method =="POST":
        form=StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
            
    return render(request, "landing.html", {'form':form, 'status' : statuses})

def remove(request):
    if request.method == "POST":
        id = request.POST['id']
        Status.objects.get(id=id).delete()
    return redirect('home')
