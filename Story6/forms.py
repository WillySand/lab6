from . import models
from django import forms
from datetime import datetime


class StatusForm(forms.ModelForm):
        status = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "charfield",
                "required" : True,
                "placeholder":"Event",
                }))
        class Meta:
                model = models.Status
                fields = ["status"]
