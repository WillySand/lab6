from django.test import TestCase,LiveServerTestCase,Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from .views import home,remove
from .models import Status
from .forms import StatusForm
from django.apps import apps
from .apps import Story6Config
from selenium.webdriver.chrome.options import Options
   

class Lab6UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'Story6')
        self.assertEqual(apps.get_app_config('Story6').name, 'Story6')
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)#test existing url
        response = Client().get('/admin')
        self.assertNotEqual(response.status_code, 200)#test url that doesn't exist
        response = Client().get('/home')
        self.assertNotEqual(response.status_code, 200)
    def test_function(self):
        foundFunc = resolve('/')
        self.assertEqual(foundFunc.func, home)
        foundFunc = resolve('/remove')
        self.assertEqual(foundFunc.func, remove)
    def test_model(self):
        status = Status.objects.create(status='ABCDEFGH')
        self.assertIsInstance(status, Status)#test object creation
    def test_form(self):
        form = StatusForm(data={'status':''})#test empty
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ["This field is required."])
        form = StatusForm(data={'status':(200*'0123456789')})#test over limit
        self.assertFalse(form.is_valid())
        form = StatusForm(data={'status':'ABCDEFGH'})#test valid
        self.assertTrue(form.is_valid())
    def test_lab6_post_success_and_render_the_result(self):
        response_post = Client().post('/', {'status':'SayaKakPewePalsu!'})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('SayaKakPewePalsu!', html_response)
    
    def test_lab6_post_failed_and_render_the_result(self):
        response_post = Client().post('/', {'status':100*'lima'})
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(100*'lima', html_response)

class AccountTestCase(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

        super(AccountTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(AccountTestCase, self).tearDown()

    def test_register(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        assert 'Status Input testing' not in selenium.page_source
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('button')
        status.send_keys('Status Input testing')
        submit.send_keys(Keys.RETURN)
        assert 'Status Input testing' in selenium.page_source
        delete = selenium.find_element_by_id('Status Input testingDel')
        delete.send_keys(Keys.RETURN)
        assert 'Status Input testing' not in selenium.page_source
     
